This org file contains all of the algorithms that I think will be useful for various programming things. There are pseudocode examples as well as implimentations in C, C++ and Rust

* Sorting

* Searching
** [[file:Pseudocode/Searching/binarysearch.html][Binary search]]

* Path finding

* Tree/Graph functions

* Others

* Languages
** C
This section contains all of the algorithms written in C
*** Sorting
*** Searching
**** [[file:C/Searching/BinarySearch.c::int%20comparisonFunction(const%20void*%20pkey,%20const%20void*%20pelem)%20{};][Binary search]]
*** Path finding
*** Tree/Graph functions
*** Others
** C++
This section contains all of the algorithms written in C++
*** Sorting
*** Searching
***** [[file:C++/Searching/BinarySearch.cpp::/%20Recursively][Binary search]]
*** Path finding
*** Tree/Graph functions
*** Others
** Rust
This section contains all of the algorithms written in Rust
*** Sorting
*** Searching
***** [[file:Rust/Searching/BinarySearch.rs::}][Binary search]]
*** Path finding
*** Tree/Graph functions
*** Others
