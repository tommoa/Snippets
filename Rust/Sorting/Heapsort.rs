// Using stdlib

use std::collections::BinaryHeap;
 
fn main() {
    let src = vec![6,2,3,6,1,2,7,8,3,2];
    let sorted= BinaryHeap::from(src).into_sorted_vec();
    println!("{:?}", sorted);
}

// Full code

fn heap_sort<T,F>(array: &mut [T], order: F)
    where F: Fn(&T,&T) -> bool 
{
    let len = array.len();
    // Create heap
    for start in (0..len/2).rev() {
        sift_down(array,&order,start,len-1)
    }
 
    for end in (1..len).rev() {
        array.swap(0,end);
        sift_down(array,&order,0,end-1)
    }
}
 
fn sift_down<T,F>(array: &mut [T], order: &F, start: usize, end: usize) 
    where F: Fn(&T,&T) -> bool
{
    let mut root = start;
    loop {
        let mut child = root * 2 + 1;
        if child > end { break; }
        if child + 1 <= end && order(&array[child], &array[child + 1]) {
            child += 1;
        }
        if order(&array[root], &array[child]) {
            array.swap(root,child);
            root = child
        } else {
            break;
        }
    }
}
