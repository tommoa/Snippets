<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#org3a0e500">1. Sorting</a></li>
<li><a href="#orgb42203a">2. Searching</a></li>
<li><a href="#org51c0df9">3. Path finding</a></li>
<li><a href="#orged50cfc">4. Tree/Graph functions</a></li>
<li><a href="#orga33708b">5. Others</a></li>
<li><a href="#org44449d7">6. Languages</a>
<ul>
<li><a href="#orgde31006">6.1. C</a></li>
<li><a href="#org4084c44">6.2. C++</a></li>
<li><a href="#orgdf434a4">6.3. Rust</a></li>
</ul>
</li>
</ul>
</div>
</div>
This project is a collection of various algorithms that can be used for various applications. Examples of the implimentations have been written in C, C++ and Rust


<a id="org3a0e500"></a>

# Sorting

-   [Quicksort](Pseudocode/Sorting/Quicksort.md)
-   [Heapsort](Pseudocode/Sorting/C/Sorting/Heapsort.c)
-   [Mergesort](Pseudocode/Sorting/Mergesort.md)


<a id="orgb42203a"></a>

# Searching

-   [Binary search](Pseudocode/Searching/binarysearch.md)


<a id="org51c0df9"></a>

# Path finding


<a id="orged50cfc"></a>

# Tree/Graph functions


<a id="orga33708b"></a>

# Others


<a id="org44449d7"></a>

# Languages


<a id="orgde31006"></a>

## C

This section contains all of the algorithms written in C

-   Sorting
    -   [Quicksort](C/Sorting/Quicksort.c)
    -   [Heapsort](C/Sorting/Heapsort.c)
-   Searching
    -   [Binary search](C/Searching/BinarySearch.c)
-   Path finding
-   Tree/Graph functions
-   Others


<a id="org4084c44"></a>

## C++

This section contains all of the algorithms written in C++

-   Sorting
    -   [Quicksort](C++/Sorting/Quicksort.cpp)
    -   [Heapsort](C++/Sorting/Heapsort.cpp)
-   Searching
    -   [Binary search](C++/Searching/BinarySearch.cpp)
-   Path finding
-   Tree/Graph functions
-   Others


<a id="orgdf434a4"></a>

## Rust

This section contains all of the algorithms written in Rust

-   Sorting
    -   [Quicksort](Rust/Sorting/Quicksort.rs)
    -   [Heapsort](Rust/Sorting/Heapsort.rs)
-   Searching
    -   [Binary search](Rust/Searching/BinarySearch.rs)
-   Path finding
-   Tree/Graph functions
-   Others

