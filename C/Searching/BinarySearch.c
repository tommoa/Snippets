// Recursively
int bsearch (int *a, int x, int i, int j) {
    if (j < i) {
        return -1;
    }
    int k = (i + j) / 2;
    if (a[k] == x) {
        return k;
    }
    else if (a[k] < x) {
        return bsearch_r(a, x, k + 1, j);
    }
    else {
        return bsearch_r(a, x, i, k - 1);
    }
}

// Iteratively
int bsearch (int *a, int n, int x) {
    int i = 0, j = n - 1;
    while (i <= j) {
        int k = (i + j) / 2;
        if (a[k] == x) {
            return k;
        }
        else if (a[k] < x) {
            i = k + 1;
        }
        else {
            j = k - 1;
        }
    }
    return -1;
}

// Standard library
#include <stdlib.h>
type* p = (type *) bsearch(&key, array, numel, sizeOfSingleElement, comparisonFunction);
int comparisonFunction(const void* pkey, const void* pelem) {};
