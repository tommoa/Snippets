This project is a collection of various algorithms that can be used for various applications. Examples of the implimentations have been written in C, C++ and Rust

* Sorting
 - [[file:Pseudocode/Sorting/Quicksort.md][Quicksort]]
 - [[file:Pseudocode/Sorting/C/Sorting/Heapsort.c)][Heapsort]]
 - [[file:Pseudocode/Sorting/Mergesort.md::```][Mergesort]]
* Searching
 - [[file:Pseudocode/Searching/binarysearch.md][Binary search]]
* Path finding
* Tree/Graph functions
* Others
* Languages
** C

This section contains all of the algorithms written in C

 - Sorting
   - [[file:C/Sorting/Quicksort.c::/%20Quicksort%20algorithm][Quicksort]]
   - [[file:C/Sorting/Heapsort.c::}][Heapsort]]
 - Searching
   - [[file:C/Searching/BinarySearch.c::int%20comparisonFunction(const%20void*%20pkey,%20const%20void*%20pelem)%20{};][Binary search]]
 - Path finding
 - Tree/Graph functions
 - Others

** C++

This section contains all of the algorithms written in C++

 - Sorting
   - [[file:C++/Sorting/Quicksort.cpp::#include%20<algorithm>%20/%20for%20std::partition][Quicksort]]
   - [[file:C++/Sorting/Heapsort.cpp][Heapsort]]
 - Searching
   - [[file:C++/Searching/BinarySearch.cpp::/%20Recursively][Binary search]]
 - Path finding
 - Tree/Graph functions
 - Others

** Rust

This section contains all of the algorithms written in Rust

 - Sorting
   - [[file:Rust/Sorting/Quicksort.rs][Quicksort]]
   - [[file:Rust/Sorting/Heapsort.rs::}][Heapsort]]
 - Searching
   - [[file:Rust/Searching/BinarySearch.rs::}][Binary search]]
 - Path finding
 - Tree/Graph functions
 - Others
