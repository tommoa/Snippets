// Recursively
template <class T> int binsearch(const T array[], int len, T what) {
	if (len == 0) return -1;
	int mid = len / 2;
	if (array[mid] == what) return mid;
	if (array[mid] < what) {
		int result = binsearch(array+mid+1, len-(mid+1), what);
		if (result == -1) return -1;
		else return result + mid+1;
	}
	if (array[mid] > what)
		return binsearch(array, mid, what);
}

// Iteratively
template <class T> int binSearch(const T arr[], int len, T what) {
	int low = 0;
	int high = len - 1;
	while (low <= high) {
		int mid = (low + high) / 2;
		if (arr[mid] > what)
			high = mid - 1;
		else if (arr[mid] < what)
			low = mid + 1;
		else
			return mid;
	}
	return -1; // indicate not found 
}


// stdlib
#include <algorithm>
bool found = std::binary_search(array, array+len, key, comparitor = null); // Does not return position, just whether it exists
